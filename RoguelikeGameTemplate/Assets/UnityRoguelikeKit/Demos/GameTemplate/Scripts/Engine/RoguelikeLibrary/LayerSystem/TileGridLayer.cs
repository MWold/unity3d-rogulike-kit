using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

//----------------------------------------------------------------- 
//                                           D E S C R I P T I O N
//
// Under heavy construction... :P
//
public class TileGridLayer : GridLayer 
{
    //------------------------------------------------------------- 
    //                                           V A R I A B L E S
    private List<GridLayer> cleared_by_layers; //Layers that can "clear" layers in this layer

    //------------------------------------------------------------- 
    //                                         P R O P E R T I E S

    //------------------------------------------------------------- 
    //                                         S T R U C T U R E S
    struct Tile
    {
        
    }

    //------------------------------------------------------------- 
    //                                       C O N S T R U C T O R
    public TileGridLayer(string name, float depth, Vector2 size)
        : base(name, depth, size)
    {
        cleared_by_layers = new List<GridLayer>();
    }

    //------------------------------------------------------------- 
    //                                   F U N C T I O N A L I T Y
    public void update()
    {
        
    }
    
    public void add_clear_layer(GridLayer layer)
    {
        cleared_by_layers.Add(layer);
    }

    //------------------------------------------------------------- 
    //                   P R I V A T E   F U N C T I O N A L I T Y
    private bool is_valid(Vector3 position)
    {
        if (position.x >= Size.x || position.z >= Size.y)
        {
            Debug.LogError("The position " + position + " is not valid for grid layer " + Name);
            return false;
        }

        if (position.x < 0 || position.z < 0)
        {
            Debug.LogError("The position " + position + " is not valid for grid layer " + Name);
            return false;
        }

        return true;
    }

    private int to_index(Vector3 position)
    {
        return Convert.ToInt32((position.z * Size.x) + position.x);
    }
}
