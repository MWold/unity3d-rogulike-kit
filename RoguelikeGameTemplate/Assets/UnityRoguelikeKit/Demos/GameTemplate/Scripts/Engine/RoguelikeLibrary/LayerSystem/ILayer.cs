
//----------------------------------------------------------------- 
//                                           D E S C R I P T I O N
//
//
public abstract class ILayer 
{
    //------------------------------------------------------------- 
    //                                           V A R I A B L E S
    private readonly string name;
    private readonly float depth; 

    //------------------------------------------------------------- 
    //                                         P R O P E R T I E S
    public string Name { get { return name; } }
    public float Depth { get { return depth; } }

    //------------------------------------------------------------- 
    //                                       C O N S T R U C T O R
    public ILayer(string name, float depth)
    {
        this.name = name;
        this.depth = depth;
    }

    //------------------------------------------------------------- 
    //                                   F U N C T I O N A L I T Y

    //------------------------------------------------------------- 
    //                   P R I V A T E   F U N C T I O N A L I T Y
}
