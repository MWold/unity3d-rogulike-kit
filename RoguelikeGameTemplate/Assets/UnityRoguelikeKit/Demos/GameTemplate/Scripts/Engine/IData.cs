using System;
using UnityEngine;
using Boomlagoon.JSON;

//----------------------------------------------------------------- 
//                                           D E S C R I P T I O N
//
//
public abstract class IData
{
    //------------------------------------------------------------- 
    //                                           V A R I A B L E S
    protected JSONObject file;

    //------------------------------------------------------------- 
    //                                         P R O P E R T I E S

    //------------------------------------------------------------- 
    //                                   F U N C T I O N A L I T Y
    public abstract void reload();

    //------------------------------------------------------------- 
    //                   P R I V A T E   F U N C T I O N A L I T Y
    protected abstract void load_file();

    protected void check_file()
    {
        if(file == null)
            load_file();
    }

    protected void load_file(string resource)
    {
        //TODO: First try to load the file from a player accessible location, 
        //TODO: then try to load it from the resources location if that fails.
        var file_content = Resources.Load(resource, typeof(TextAsset)) as TextAsset;
        if(file_content == null)
            throw new System.InvalidOperationException("Failed to load " + resource);

        file = JSONObject.Parse(file_content.text);
        if (file == null)
            throw new System.InvalidOperationException("Failed to load " + resource);
    }

    protected void reload_file(string resource)
    {
        //Since this will be called at runtime, we don't want to use exceptions, but rather
        //not change the data in the parsed file object if loading now fails for some reason,
        //and just print out the error as a debug log.
        var file_content = Resources.Load(resource, typeof(TextAsset)) as TextAsset;
        if (file_content == null)
        {
            Debug.LogError("Failed to reload " + resource);
            return;
        }

        var temporary_file = JSONObject.Parse(file_content.text);
        if (temporary_file == null)
        {
            Debug.LogError("Failed to reload " + resource);
            return;
        }

        //We successfully reloaded the data
        file = temporary_file;
    }

    protected Boomlagoon.JSON.JSONValue get_value(string key, string resource)
    {
        check_file();
        var value = file.GetValue(key);
        if (value == null)
            throw new System.InvalidOperationException(
                "Could not find " + key + " value in " + resource);

        return value;
    }

    protected Boomlagoon.JSON.JSONArray get_array(string key, string resource)
    {
        check_file();
        var array = file.GetArray(key);
        if (array == null)
            throw new System.InvalidOperationException(
                "Could not find " + key + " array in " + resource);

        return array;
    }

    protected Boomlagoon.JSON.JSONObject get_object(string key, string resource)
    {
        check_file();
        var obj = file.GetObject(key);
        if (obj == null)
            throw new System.InvalidOperationException(
                "Could not find " + key + " object in " + resource);

        return obj;
    }

    protected Color32 string_to_Color32(string str)
    {
        var color = new Color32();

        var elements = str.Split(' ');
        if (elements.Length == 3)
        {
            color.r = Convert.ToByte(elements[0]);
            color.g = Convert.ToByte(elements[1]);
            color.b = Convert.ToByte(elements[2]);
        }
        else if (elements.Length == 4)
        {
            color.r = Convert.ToByte(elements[0]);
            color.g = Convert.ToByte(elements[1]);
            color.b = Convert.ToByte(elements[2]);
            color.a = Convert.ToByte(elements[3]);
        }
        else
        {
            Debug.LogError("Failed to convert string to Color32: " + str);
        }

        return color;
    }
	
	protected Color32 array_to_Color32(JSONArray array)
    {
        //Make sure the array is of the correct size
        if (array.Length > 4 || array.Length < 3)
        {
            //We return a default color instead of throwing an exception, since this function
            //can be called at runtime!
            Debug.LogError(
                "Tried to convert a json array to Color32, but the array length was not supported! It needs to contain 3 or 4 entries.");
            return new Color32();
        }

        int i = 0;
        Color32 color = new Color32();
        foreach (var value in array)
        {
            //We only expect to find numbers in this array!
            if (value.Type != JSONValueType.Number)
            {
                Debug.LogError(
                    "Tried to convert a json array to Color32, but it contained values that were not numbers!");
                return new Color32();
            }

            if (i == 0) color.r = Convert.ToByte(value.Number);
            if (i == 1) color.g = Convert.ToByte(value.Number);
            if (i == 2) color.b = Convert.ToByte(value.Number);
            if (i == 3) color.a = Convert.ToByte(value.Number);

            i++;
        }

        return color;
    }
}
