
using UnityEngine;

//----------------------------------------------------------------- 
//                                           D E S C R I P T I O N
//
//
public class Game
{
    //------------------------------------------------------------- 
    //                                           V A R I A B L E S
    private static Game instance;
    private bool is_started;

    private GameData data;

    private CharacterSelectionScreen character_selection_screen;
    private GameScreen game_screen;
    
    private IScreen active_screen;
    

    //------------------------------------------------------------- 
    //                                         P R O P E R T I E S
    public static Game Instance
    {
        get
        {
            if (instance == null)
                instance = new Game();

            return instance;
        }
    }

    //------------------------------------------------------------- 
    //                                   F U N C T I O N A L I T Y
    private Game()
    {
        is_started = false;
    }

    public void start()
    {
        if (is_started) return;
        is_started = true;

        data = new GameData();

        character_selection_screen = new CharacterSelectionScreen(this);

        var start_screen = data.get_start_screen();
        if(start_screen == "character_selection")
            change_to_character_selection_screen();
        else if(start_screen == "game")
            change_to_game_screen();
        else
            change_to_game_screen(); //Fall back to game screen
    }

    public void update()
    {
        var elapsed_time = Time.deltaTime;

        if (active_screen != null) active_screen.update(elapsed_time);
    }

    public void late_update()
    {
    }

    public void change_to_character_selection_screen()
    {
        if (!is_started) return;

        active_screen = character_selection_screen.set_active();
    }

    public void change_to_game_screen()
    {
        if (!is_started) return;

        //We don't instantiate the game screen before we actually need to
        if(game_screen == null)
            game_screen = new GameScreen(this);

        active_screen = game_screen.set_active();
    }
}
