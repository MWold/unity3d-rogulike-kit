﻿using System;
using System.Linq;
using URLK;
using UnityEngine;
using System.Collections;

public class FieldOfViewDemo : MonoBehaviour 
{
    //in order to be in line with GUI coordinate pairs, this appears to be sideways in this style constructor.
    private static string[] DEFAULT_MAP =
    {
        "øøøøøøøøøøøøøøøøøøøøøøøøøøøøøøøøøøø########################################øøøøøøøøøøøøøøøøøøøøøøøøø",
        "øøøøøøøøøøøøøøøøøø#########øøøøøøøø#,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,#..m.mmmmmmmmmmmmmm..m...ø",
        "øøøøøøøøøøø########.......##øøøøøøø#,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,#.mmTmmmmmmmmmmmmmm......ø",
        "øøøøø#######.......₤.......###øøøøø#¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸,TTT¸,,¸¸¸¸¸¸m.TmmmmmmmmmTmmmmmmm..m..ø",
        "øøø###₤₤₤₤₤..................#øøøøø#¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸¸,TTT,,¸¸¸¸¸¸mmmmm≈≈≈mm..mmmmmmmmm....ø",
        "øøø#₤₤₤₤₤.₤₤....₤............##øøøø#¸¸¸¸¸¸¸¸¸¸¸TTT¸¸¸¸¸¸¸¸¸¸¸¸¸,¸¸,,¸¸¸¸¸¸mmm≈≈≈≈≈mm.m.mmmmmmm.....ø",
        "øø##.₤₤₤₤₤₤₤₤.................####ø#¸¸¸¸¸¸¸TTTTTT¸¸¸¸¸¸¸¸¸¸¸¸¸¸c,¸,,¸¸¸¸¸¸mm≈≈m≈mmmmmmmmm≈≈≈m..m...ø",
        "øø#..₤₤₤₤₤₤₤.....................###¸¸¸TTTTTT¸¸¸¸¸¸¸¸¸¸¸¸¸¸ct¸ctc,,,¸¸¸¸¸¸m≈≈mmmmmmmTmmm≈≈≈≈≈≈≈≈...ø",
        "øø#...₤₤₤₤₤............₤............¸¸¸¸¸TTTTTT¸¸¸¸¸¸¸¸¸¸¸¸ctt¸c¸¸,,¸¸¸¸¸¸mm≈≈mmmmmTmmm≈≈≈m≈≈mmmm..ø",
        "øø#.₤₤₤₤₤₤₤...........₤₤............¸¸TTTTT¸¸TTT¸¸¸¸¸¸¸¸¸¸¸¸cc¸¸¸¸,,¸¸¸¸¸¸mmm≈mmmmmmmmm≈≈mTm≈≈mmm..ø",
        "ø##₤₤₤₤₤₤₤₤₤.......................###############################,,¸¸¸¸S¸#mmmm≈m≈≈≈mm≈≈≈≈mmmmm≈mmmø",
        "ø#₤₤₤₤₤₤₤₤₤₤..₤....................#.....#.....#.....#.....#.....#+/#¸¸¸S¸#.T.mm≈≈≈≈≈≈≈≈≈mmmTmmmmm.ø",
        "ø#₤₤₤₤..₤₤₤₤₤......................#.....#.....#.....#.....#.....#..#¸¸¸¸¸#.TT.mmm≈≈≈≈≈≈≈≈mmm.mmT.mø",
        "ø#.₤₤₤.₤₤₤₤₤₤......₤...............#.....#.....#.....#.....#.....#..#######.TTTTmmm≈≈≈≈≈≈mmT..mmm.mø",
        "ø#₤₤₤₤₤₤₤₤₤........₤...............#.....#.....#.....#.....#.....#/+#.....#..TTmmmm≈≈≈≈≈≈TTmTmmTmmmø",
        "ø#₤₤₤₤₤₤₤₤₤₤.......................#.....#.....#.....#.....#.....#..#.....#..T.mmmm≈≈≈≈≈≈≈mmTmTm≈≈≈ø",
        "ø#₤₤₤₤₤₤₤₤₤₤.......................#.....###+#####+#####/#####+###..+.....#...Tmmmmm≈≈≈≈≈≈≈≈mmmmT≈≈ø",
        "##₤₤₤₤₤₤₤₤₤₤.......................#######..........................#.....#mT..mmmmmm≈≈≈≈≈≈≈mmm≈≈≈mø",
        "#₤₤..₤₤₤₤₤₤₤₤......................#.....#..........................#.....#m..mmmmmmm≈≈≈≈≈≈≈Tm≈≈≈mmø",
        "#₤..₤₤₤₤₤₤₤₤₤......................#.....#..........................#######..mmmmmmmmm≈≈≈≈≈≈m≈≈mmmmø",
        "#..................................#.....#...####################...#...#E#..mmm.mmmmm≈≈≈≈≈≈≈≈mmmmmø",
        "#..................................#.....#...+..E#..............#.../.../.#.......mmmm≈≈≈≈≈mmmmmmmmø",
        "#..................................#.....#...#####..............#...#...#E#........mm≈≈≈≈≈mmmmmmmmmø",
        "#..................................#.....#...#..................#...#######...m.....m≈≈≈≈mmmmmmmmmmø",
        "#..................................#.....#...#..................#.........+......mmmm≈≈mmm....mm≈≈mø",
        "#..................................#.....#...#..................#........./...uu...um≈≈mu.....m≈≈≈mø",
        "#..................................#.....#...#..................#...#+###+#..uuuuuuuu≈≈uu.u.ummmmuuø",
        "#..................................#.....#...#.................##...#..#c.#uuuuuuuuuu≈≈uuuAuuuuuuuuø",
        "#..................................#.....#...#................#.#...#E.#t.#uuuuuAuuA≈≈≈≈≈uuuuuuuuuuø",
        "#..................................#.....#...#...............#..#...#E<#c.#uuAuAuuu≈≈≈≈≈≈≈AuAAuuAuuø",
        "#..................................#.....#...#.............##.../...#######uAuAAA≈≈≈≈≈≈≈≈≈≈AAAAAAAuø",
        "#..................................#.....#...#............#.....#...#.....#AAAuA≈≈≈≈≈≈≈≈≈≈≈AAAAAAAAø",
        "#..................................#.....#...#............#.....#...#.....#AAAA≈≈≈≈≈≈≈≈≈≈≈≈≈AAAAAAAø",
        "#..................................#.....#...####################...#.....#AAAAu≈≈≈≈≈≈≈≈≈≈≈≈≈≈AAAAAø",
        "#..................................#.....#.......EEEEEEEEEEE........#.....#AAAAuu.≈≈≈≈mmm≈≈≈≈≈AAuAAø",
        "#..................................#.....#..........................#.....#AAAuuuu≈≈≈≈≈mm≈≈≈≈AAuuAAø",
        "#..................................#.....#..........................#.....#AAAAuuuu≈≈≈≈≈≈≈≈≈AAuuuAAø",
        "#..................................#.....####+###+#####+#####+#####/#.....#AAAAAAAuu..≈≈≈≈≈AAAAuAAAø",
        "#..................................#.....#E.+.#.....#.....#.....#.........#AAAAAAAAA.AAA≈≈AAAAAAAAAø",
        "#..................................#.....####.#.....#.....#.....#tttt+#...#AAAAAAAA..AAAuuu.uAAAAAAø",
        "#..................................#.....#E.+.#.....#.....#.....#..c..#...#AAAAAAA....AAAAu..uAAAAAø",
        "#..................................#.....####.#.....#.....#.....###..E#...#AAAAAA...AAAAAuuu.uAAAAAø",
        "#..................................#.....#E.+.#.....#.....#.....#E+.EE#...#AAAAAAAAAAAAAAAAAuuAAAAAø",
        "###########################################################################AAAAAAAAAAAAAAAAAAAAAAAAø"
    };
    private DemoCell[,] map;
    private float[,] light;
    private float[,] resistances;
    private int width = DEFAULT_MAP[0].Length, height = DEFAULT_MAP.Length;
    private int cellWidth, cellHeight, locx, locy;
    private LOSSolver los;
    private SColor litNear, litFar, replaceForeground = SColor.COBALT;
    private bool lightBackground = false;//if true then the defaultForeground will be used for objects and the background will be lit
    private float lightForce; //controls how far the light will spread

	// Use this for initialization
	private void Start () 
    {
	    map = new DemoCell[width,height];
        resistances = new float[width,height];
        for (int x = 0; x < width; x++) 
        {
            for (int y = 0; y < height; y++) 
            {
                char c = DEFAULT_MAP[y][x];
                map[x,y] = buildCell(c);
                resistances[x,y] = map[x,y].resistance;
            }
        }

        //put start location at middle of map
        locx = width / 2;
        locy = height / 2;
	}
	
	// Update is called once per frame
	private void Update ()
	{
	    Vector2? direction = null;
	    if (Input.GetKeyDown(KeyCode.Keypad4))
	        direction = Direction.LEFT;
        else if (Input.GetKeyDown(KeyCode.Keypad6))
            direction = Direction.RIGHT;
        else if (Input.GetKeyDown(KeyCode.Keypad8))
            direction = Direction.UP;
        else if (Input.GetKeyDown(KeyCode.Keypad2))
            direction = Direction.DOWN;
        else if (Input.GetKeyDown(KeyCode.Keypad1))
            direction = Direction.DOWN_LEFT;
        else if (Input.GetKeyDown(KeyCode.Keypad3))
            direction = Direction.DOWN_RIGHT;
        else if (Input.GetKeyDown(KeyCode.Keypad7))
            direction = Direction.UP_LEFT;
        else if (Input.GetKeyDown(KeyCode.Keypad9))
            direction = Direction.UP_RIGHT;
        else if (Input.GetKeyDown(KeyCode.Keypad5))
            direction = Direction.NONE;

        if(direction == null)
            return;
	    
        move(direction.Value);
	}

    private void clear()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                //display.placeCharacter(x, y, map[x][y].representation, map[x][y].color, SColor.BLACK);
            }
        }
        //display.refresh();
    }

    /**
     * Builds a cell based on the character in the map.
     *
     * @param c
     * @return
     */
    private DemoCell buildCell(char c)
    {
        float resistance = 0f;//default is transparent
        SColor color;
        switch (c)
        {
            case '.'://stone ground
                color = SColor.SLATE_GRAY;
                break;
            case '¸'://grass
                color = SColor.GREEN;
                break;
            case ','://pathway
                color = SColor.STOREROOM_BROWN;
                c = '.';
                break;
            case 'c':
                color = SColor.SEPIA;
                break;
            case '/':
                color = SColor.BROWNER;
                break;
            case '≈':
                color = SColor.AZUL;
                break;
            case '<':
            case '>':
                color = SColor.SLATE_GRAY;
                break;
            case 't':
                color = SColor.BROWNER;
                resistance = 0.3f;
                break;
            case 'm':
                color = SColor.BAIKO_BROWN;
                resistance = 0.1f;
                break;
            case 'u':
                color = SColor.TAN;
                resistance = 0.2f;
                break;
            case 'T':
            case '₤':
                color = SColor.FOREST_GREEN;
                resistance = 0.7f;
                break;
            case 'E':
                color = SColor.SILVER;
                resistance = 0.8f;
                break;
            case 'S':
                color = SColor.BREWED_MUSTARD_BROWN;
                resistance = 0.9f;
                break;
            case '#':
                color = SColor.SLATE_GRAY;
                resistance = 1f;
                break;
            case '+':
                color = SColor.BROWNER;
                resistance = 1f;
                break;
            case 'A':
                color = SColor.ALICE_BLUE;
                resistance = 1f;
                break;
            case 'ø':
                c = ' ';
                color = SColor.BLACK;
                resistance = 1f;
                break;
            default://opaque items
                resistance = 1f;//unknown is opaque
                color = SColor.DEEP_PINK;
                break;
        }
        return new DemoCell(resistance, c, color);
    }

    private void move(Vector2 dir)
    {
        int x = locx + Convert.ToInt32(dir.x);
        int y = locy + Convert.ToInt32(dir.y);

        //check for legality of move based solely on map boundary
        if (x >= 0 && x < width && y >= 0 && y < height)
        {
            locx = x;
            locy = y;
            doFOV(x, y);
        }
    }

    /**
     * Performs the Field of View process
     *
     * @param startx
     * @param starty
     */
    private void doFOV(int startx, int starty)
    {
        //manually set the radius to equal the force
        //lightForce = panel.radiusSlider.getValue();
        //litNear = SColorFactory.asSColor(panel.castColorPanel.getBackground().getRGB());
        //litFar = SColorFactory.asSColor(panel.fadeColorPanel.getBackground().getRGB());
        //light = panel.getFOVSolver().calculateFOV(resistances, startx, starty, 1f, 1/lightForce, panel.getStrategy());
        //        SColorFactory.emptyCache();//uncomment to check perfomance differences in flooring values
        SColorFactory.addPallet("light", SColorFactory.asGradient(litNear, litFar));

        //repaint the level with new light map -- Note that in normal use you'd limit this to just elements that changed
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (light[x,y] > 0f)
                {
                    if (lightBackground)
                    {
                        //display.placeCharacter(x, y, map[x,y].representation, replaceForeground,
                        //                       SColorFactory.fromPallet("light", 1 - light[x,y]));
                    }
                    else
                    {
                        var radius = Math.Sqrt((x - startx)*(x - startx) + (y - starty)*(y - starty));
                        var brightt = 1 - light[x,y];
                        var cellLightt = SColorFactory.fromPallet("light", brightt);
                        //SColor objectLight = SColorFactory.blend(map[x,y].color, cellLightt, getTint(radius));
                       // display.placeCharacter(x, y, map[x][y].representation, objectLight);
                    }
                }
                else
                {
                    //display.clearCell(x, y);
                }
            }
        }

        //put the player at the origin of the FOV
        var bright = 1 - light[startx,starty];
        var cellLight = SColorFactory.fromPallet("light", bright);
        //var objectLight = SColorFactory.blend(SColor.ALICE_BLUE, cellLight, getTint(0f));
        if (lightBackground)
        {
            //display.placeCharacter(startx, starty, '@', replaceForeground, objectLight);
        }
        else
        {
            //display.placeCharacter(startx, starty, '@', objectLight);
        }
        //display.refresh();
    }

    /**
     * Custom method to determine tint based on radius as well as general tint
     * factor.
     *
     * @param radius
     * @return
     */
    public float getTint(double radius)
    {
        float tint = 0;// = panel.tintSlider.getValue()/100f;
        tint = (float) (0f + tint*radius); //adjust tint based on distance
        return tint;
    }

    /**
     * Performs the Line of Sight calculation and paints target square with a
     * green background if it can be reached or a red if not.
     *
     * @param startx
     * @param starty
     * @param endx
     * @param endy
     */

    private void doLOS(int startx, int starty, int endx, int endy)
    {
        //los = panel.getLOSSolver();

        //run the LOS calculation
        var visible = los.isReachable(resistances, startx, starty, endx, endy);
        var path = los.getLastPath();

        //draw out background for path followed
        foreach (var p in path.ToList())
        {
            //display.setCellBackground(p.x, p.y, SColor.BLUE_GREEN_DYE);
        }

        //mark the start location
        //display.setCellBackground(startx, starty, SColor.AMBER_DYE);

        //mark end point
        if (visible)
        {
            //display.setCellBackground(endx, endy, SColor.BRIGHT_GREEN);
        }
        else
        {
            //display.setCellBackground(endx, endy, SColor.RED_PIGMENT);
        }
        //display.refresh();
    }
}
