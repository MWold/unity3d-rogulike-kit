using System;
using UnityEngine;
using System.Collections;

//DISCLAIMER:
//This is a port of Eben Howard's SquidLib (in Java).
//

/**
 * Acts as a wrapper which fully respects translucency and lights based on
 * another FOVSolver.
 *
 * @author Eben Howard - http://squidpony.com - howard@squidpony.com
 * @author P�l Trefall - URLK - admin@ptrefall.com
 */
namespace URLK
{
    public class TranslucenceWrapperFOV : FOVSolver
    {
        public enum RayType
        {
            PRIMARY, SECONDARY, TERTIARY
        };
        private FOVSolver fov;// = new ShadowFOV();
        private float[,] lightMap, resistanceMap, shadowMap;
        private int width, height/*, startx, starty*/;
        private RadiusStrategy rStrat;
        private float decay;

        /**
         * Uses default ShadowFOV to create lit area mapping
         */
        public TranslucenceWrapperFOV()
        {
        }

        /**
         * Uses provided FOVSolver to create lit area mapping
         *
         * @param fov
         */
        public TranslucenceWrapperFOV(FOVSolver fov)
        {
            this.fov = fov;
        }

        public float[,] calculateFOV(float[,] resistanceMap, int startx, int starty, float force, float decay, RadiusStrategy radiusStrategy)
        {
            width = resistanceMap.Length;
            height = resistanceMap.GetLength(0);
            //this.startx = startx;
            //this.starty = starty;
            this.decay = decay;
            this.resistanceMap = resistanceMap;
            this.rStrat = radiusStrategy;
            lightMap = new float[width,height];
            shadowMap = fov.calculateFOV(resistanceMap, startx, starty, force, decay, radiusStrategy);

            lightMap[startx,starty] = force; //start out at full force
            for (var i = 0; i < Direction.OUTWARDS.Length; i++)
            {
                var dir = Direction.OUTWARDS[i];
                pushLight(Convert.ToInt32(startx + dir.x), Convert.ToInt32(starty + dir.y), force + decay, dir, dir, RayType.PRIMARY);
            }

            return lightMap;
        }

        /**
         * Pushes light outwards based on the light coming into this cell.
         *
         * Tertiary light should only light up opaque objects, with more opaque
         * objects requiring less light to be lit up.
         *
         * @param x
         * @param y
         * @param light the amount of light coming into this tile
         * @param tertiary whether the light coming in is tertiary
         */
        private void pushLight(int x, int y, float light, Vector2 dir, Vector2 previous, RayType type)
        {
            if (light <= 0 || x < 0 || x >= width || y < 0 || y >= height || shadowMap[x,y] <= 0 || lightMap[x,y] >= light)
            {
                return; //out of light, off the edge, base fov not lit, or already well lit
            }

            lightMap[x,y] = light; //apply passed in light

            if (type == RayType.PRIMARY)
            {
                //push primary ray
                float radius = rStrat.radius(x, y, x + dir.x, y + dir.y);
                float brightness = light;
                brightness *= (1 - resistanceMap[x,y]); //light is reduced by the portion of the square passed through
                brightness -= radius*decay; //reduce by the amount of decay from passing through
                pushLight(Convert.ToInt32(x + dir.x), Convert.ToInt32(y + dir.y), brightness, dir, dir, RayType.PRIMARY);

                var pushing = Direction.getDirection(Direction.clockwise(dir));
                radius = rStrat.radius(x, y, x + pushing.x, y + pushing.y);
                brightness = light*(1 - resistanceMap[x,y]);
                    //light is reduced by the portion of the square passed through
                brightness -= radius*decay; //reduce by the amount of decay from passing through
                pushLight(Convert.ToInt32(x + pushing.x), Convert.ToInt32(y + pushing.y), brightness, dir, pushing, RayType.SECONDARY);

                pushing = Direction.getDirection(Direction.counterClockwise(dir));
                radius = rStrat.radius(x, y, x + pushing.x, y + pushing.y);
                brightness = light*(1 - resistanceMap[x,y]);
                    //light is reduced by the portion of the square passed through
                brightness -= radius*decay; //reduce by the amount of decay from passing through
                pushLight(Convert.ToInt32(x + pushing.x), Convert.ToInt32(y + pushing.y), brightness, dir, pushing, RayType.SECONDARY);
            }
            else
            {
                //type == SECONDARY at this point
                //push pass-through secondary ray
                var pushing = previous; //redirect to previous' previous direction
                float radius = rStrat.radius(x, y, x + pushing.x, y + pushing.y);
                float brightness = light*(1 - resistanceMap[x,y]);
                    //light is reduced by the portion of the square passed through
                brightness -= radius*decay; //reduce by the amount of decay from passing through
                pushLight(Convert.ToInt32(x + pushing.x), Convert.ToInt32(y + pushing.y), brightness, dir, pushing, RayType.SECONDARY);

                //now push through tertiary rays, first just continues in direction passed in
                radius = rStrat.radius(x, y, x + dir.x, y + dir.y);
                brightness = light*(1 - resistanceMap[x,y]);
                    //light is reduced by the portion of the square passed through
                brightness -= radius*decay; //reduce by the amount of decay from passing through
                pushLight(Convert.ToInt32(x + dir.x), Convert.ToInt32(y + dir.y), brightness, dir, pushing, RayType.SECONDARY);
                if (Direction.clockwise(previous) == Direction.getDirection(dir))
                {
                    pushing = Direction.getDirection(Direction.clockwise(previous));
                }
                else
                {
                    pushing = Direction.getDirection(Direction.counterClockwise(previous));
                }

                radius = rStrat.radius(x, y, x + pushing.x, y + pushing.y);
                brightness = light*(1 - resistanceMap[x,y]);
                    //light is reduced by the portion of the square passed through
                brightness -= radius*decay; //reduce by the amount of decay from passing through
                pushLight(Convert.ToInt32(x + pushing.x), Convert.ToInt32(y + pushing.y), brightness, dir, pushing, RayType.SECONDARY);
            }
        }

        public float[,] calculateFOV(float[,] resistanceMap, int startx, int starty, float radius)
        {
            return calculateFOV(resistanceMap, startx, starty, 1, 1 / radius, new BasicRadiusStrategy(BasicRadiusStrategy.STRATEGY.CIRCLE));
        }
    }
}
