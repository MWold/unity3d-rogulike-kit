using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

//DISCLAIMER:
//This is a port of Eben Howard's SquidLib (in Java).
//

/**
 * Uses Wu's Algorithm as modified by Elias to draw the line.
 *
 * The side view parameter is how far along the antialiased path to consider
 * when determining if the far end can be seen, from 0.0 to 1.0 with 1.0
 * considering the entire line. The closer to 1.0 this value is, the more
 * complete and symmetrical end-to-end the line is, but the computation cost
 * also increases.
 *
 * If undesired artifacts are seen when running the lines (missing squares
 * perhaps), try increasing the sideview value.
 *
 * @author Eben Howard - http://squidpony.com - howard@squidpony.com
 * @author P�l Trefall - URLK - admin@ptrefall.com
 */
namespace URLK
{
    public class EliasLOS : LOSSolver
    {
        private Queue<Vector2> lastPath = new Queue<Vector2>();
        private float sideview = 0.75f;

        /**
         * Creates this solver with the default side view parameter.
         */
        public EliasLOS() 
        {
        }

        /**
         * Creates this solver with the provided value for how far along the
         * antialiased line the antialiased portion will be considered.
         *
         * @param sideview
         */
        public EliasLOS(float sideview) 
        {
            this.sideview = sideview;
        }

        public float getSideview() 
        {
            return sideview;
        }

        public void setSideview(float sideview) 
        {
            this.sideview = sideview;
        }

        public bool isReachable(float[,] resistanceMap, int startx, int starty, int targetx, int targety, float force, float decay, RadiusStrategy radiusStrategy)
        {
            var path = Elias.line(startx, starty, targetx, targety);
            lastPath = new Queue<Vector2>(path); //save path for later retreival

            BresenhamLOS los1 = new BresenhamLOS(),
                         los2 = new BresenhamLOS();

            float checkRadius = radiusStrategy.radius(startx, starty)*0.75f;
            while (path.Count > 0)
            {
                var p = path[0];
                path.RemoveAt(0);

                //if a non-solid midpoint on the path can see both the start and end, consider the two ends to be able to see each other
                if (resistanceMap[Convert.ToInt32(p.x), Convert.ToInt32(p.y)] < 1
                    && radiusStrategy.radius(startx, starty, p.x, p.y) < checkRadius
                    && los1.isReachable(resistanceMap, Convert.ToInt32(p.x), Convert.ToInt32(p.y), targetx, targety,
                                     force - (radiusStrategy.radius(startx, starty, p.x, p.y)*decay), decay,
                                     radiusStrategy)
                    && los2.isReachable(resistanceMap, startx, starty, Convert.ToInt32(p.x), Convert.ToInt32(p.y), force, decay, radiusStrategy))
                {

                    //record actual sight path used
                    lastPath = new Queue<Vector2>(los1.getLastPath());
                    
                    while(los2.getLastPath().Count > 0)
                        lastPath.Enqueue(los2.getLastPath().Dequeue());

                    return true;
                }
            }

            return false; //never got to the target point
        }

        public bool isReachable(float[,] resistanceMap, int startx, int starty, int targetx, int targety)
        {
            return isReachable(resistanceMap, startx, starty, targetx, targety, float.MaxValue, 0f, new BasicRadiusStrategy(BasicRadiusStrategy.STRATEGY.CIRCLE));
        }

        public Queue<Vector2> getLastPath()
        {
            return lastPath;
        }
    }
}
