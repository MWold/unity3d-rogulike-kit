using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

//DISCLAIMER:
//This is a port of Eben Howard's SquidLib (in Java).
//

/**
 * Provides a means to generate Bresenham lines in 2D and 3D.
 *
 * @author Eben Howard - http://squidpony.com - howard@squidpony.com
 * @author Lewis Potter
 * @author P�l Trefall - URLK - admin@ptrefall.com
 */
namespace URLK
{
    public class Bresenham
    {
        /**
         * Prevents any instances from being created
         */
        private Bresenham()
        {
            
        }

        /**
         * Generates a 2D Bresenham line between two points.
         *
         * @param a
         * @param b
         * @return
         */
        public static Queue<Vector2> line2D(Vector2 a, Vector2 b)
        {
            return line2D((int)a.x, (int)a.y, (int)b.x, (int)b.y);
        }

        /**
         * Generates a 2D Bresenham line between two points.
         *
         * @param startX
         * @param startY
         * @param endX
         * @param endY
         * @return
         */

        public static Queue<Vector2> line2D(int startX, int startY, int endX, int endY)
        {
            var line = new Queue<Vector2>();
            var found = line3D(startX, startY, 0, endX, endY, 0);
            while (found.Count > 0)
                line.Enqueue(found.Dequeue());

            return line;
        }

        /**
     * Generates a 3D Bresenham line between two points.
     *
     * @param a Point to start from. This will be the first element of the list
     * @param b Point to end at. This will be the last element of the list.
     * @return A list of points between a and b.
     */
        public static Queue<Vector3> line3D(Vector3 a, Vector3 b)
        {
            return line3D((int)a.x, (int)a.y, (int)a.z, (int)b.x, (int)b.y, (int)b.z);
        }

        /**
         * Generates a 3D Bresenham line between the given coordinates.
         *
         * @param startx
         * @param starty
         * @param startz
         * @param endx
         * @param endy
         * @param endz
         * @return
         */

        public static Queue<Vector3> line3D(int startx, int starty, int startz, int endx, int endy, int endz)
        {
            var result = new Queue<Vector3>();

            int dx = endx - startx;
            int dy = endy - starty;
            int dz = endz - startz;

            int ax = Math.Abs(dx) << 1;
            int ay = Math.Abs(dy) << 1;
            int az = Math.Abs(dz) << 1;

            int signx = (int) Math.Sign(dx);
            int signy = (int) Math.Sign(dy);
            int signz = (int) Math.Sign(dz);

            int x = startx;
            int y = starty;
            int z = startz;

            int deltax, deltay, deltaz;
            if (ax >= Math.Max(ay, az)) /* x dominant */
            {
                deltay = ay - (ax >> 1);
                deltaz = az - (ax >> 1);
                while (true)
                {
                    result.Enqueue(new Vector3(x, y, z));
                    if (x == endx)
                    {
                        return result;
                    }

                    if (deltay >= 0)
                    {
                        y += signy;
                        deltay -= ax;
                    }

                    if (deltaz >= 0)
                    {
                        z += signz;
                        deltaz -= ax;
                    }

                    x += signx;
                    deltay += ay;
                    deltaz += az;
                }
            }
            else if (ay >= Math.Max(ax, az)) /* y dominant */
            {
                deltax = ax - (ay >> 1);
                deltaz = az - (ay >> 1);
                while (true)
                {
                    result.Enqueue(new Vector3(x, y, z));
                    if (y == endy)
                    {
                        return result;
                    }

                    if (deltax >= 0)
                    {
                        x += signx;
                        deltax -= ay;
                    }

                    if (deltaz >= 0)
                    {
                        z += signz;
                        deltaz -= ay;
                    }

                    y += signy;
                    deltax += ax;
                    deltaz += az;
                }
            }
            else if (az >= Math.Max(ax, ay)) /* z dominant */
            {
                deltax = ax - (az >> 1);
                deltay = ay - (az >> 1);
                while (true)
                {
                    result.Enqueue(new Vector3(x, y, z));
                    if (z == endz)
                    {
                        return result;
                    }

                    if (deltax >= 0)
                    {
                        x += signx;
                        deltax -= az;
                    }

                    if (deltay >= 0)
                    {
                        y += signy;
                        deltay -= az;
                    }

                    z += signz;
                    deltax += ax;
                    deltay += ay;
                }
            }
            return result;
        }
    }
}